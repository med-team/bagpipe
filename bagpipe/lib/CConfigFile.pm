#Copyright 2012 William Valdar
#This file is part of bagpipe.

#bagpipe is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#bagpipe is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with bagpipe.  If not, see <http://www.gnu.org/licenses/>.

use strict;
package CConfigFile;
use base qw(CCloneable);
use English;
use StdDefs;
use Assert;
use FileIoHelper qw(OpenFilesForReading OpenFilesForWriting);

my $KEY_FILE_NAME = 'CConfigFile::KEY_FILE_NAME';

sub new($$)
{
    my($class, $file) = @ARG;

    my $table = {};

    # file table from file
    _ReadConfig(OpenFilesForReading($file), $table);

    $table->{$KEY_FILE_NAME} = $file;

    # return as object
    return bless $table, $class;
}

sub add($$$)
{
    my($this, $key, $value) = @ARG;
    if ($this->has($key))
    {
        Fatal("can only add() new keys");
    }
    $this->put($key, $value);
}

sub get($$)
{
    my($this, $key) = @ARG;
    if ($this->has($key))
    {
        return $this->{$key};
    }
    else
    {
        Fatal("config key \'$key\' not found");
    }
}

sub getKeys($)
{
    my $this = shift;
    my @keys = ();
    for my $key (keys %$this)
    {
        next if $key eq $KEY_FILE_NAME;
        push @keys, $key;
    }
    return @keys;
}

sub getName($)
{
    my $this = shift;
    return $this->{$KEY_FILE_NAME};
}

sub has($$)
{
    my($this, $key) = @ARG;
    return exists $this->{$key};
}

sub put($$$)
{
    my($this, $key, $value) = @ARG;
    my $oldValue = $this->{$key};
    $this->{$key} = $value;
    return $oldValue;
}

sub reload($)
{
    my $this = shift;
    _ReadConfig(OpenFilesForReading($this->getName), $this);
    return $this;
}

sub remove($$)
{
    my($this, $key) = @ARG;
    if ($this->has($key))
    {
        return delete $this->{$key};
    }
    else
    {
        Fatal("cannot delete, key \'$key\' doesn't exist");
    }
}

sub set($$$)
{
    my($this, $key, $value) = @ARG;
    unless ($this->has($key))
    {
        Fatal("can only set() existing values");
    }
    return $this->put($key, $value);
}

sub setName($$)
{
    my($this, $file) = @ARG;
    $this->{$KEY_FILE_NAME} = $file;
    return $this;
}

sub save($)
{
    my $this = shift;
    _SaveConfig($this, $this->getName);
}

sub _ReadConfig($$)
{
    my($ist, $table) = @ARG;
    # read config file
    while (my $line = $ist->getline)
    {
        if ($line =~ m/^(\S+)\s+(\S.*?)\s*$/)
        {
            $table->{$1} = $2;
        }
    }
}

sub _SaveConfig($$)
{
    my($this, $file) = @ARG;
    my $ost = OpenFilesForWriting($file);
    $this->write($ost);
}

sub write($$)
{
    my($this, $ost) = @ARG;
    for my $key (sort keys %$this)
    {
        next if $key eq $KEY_FILE_NAME;
        $ost->print("$key\t".$this->{$key}."\n");
    }
}

true;


