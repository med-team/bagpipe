#Copyright 2012 William Valdar
#This file is part of bagpipe.

#bagpipe is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#bagpipe is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with bagpipe.  If not, see <http://www.gnu.org/licenses/>.

library(bagpipe.backend)
if(cmdline.flag("devel"))
{
  detach("package:bagpipe.backend")
  library(bagpipe)
}

options(warn=1)
cat(commandArgs(), "\n")
## start clock
(my.start <- Sys.time())

#--- BODY ---

config <- bagpipe.read.configfile(cmdline.option("configfile"))
h <- happy.load.genome(configfile.string(config, "genome.cache.dir"))
num.boots <- cmdline.integer("num.posboots")
bootfile   <- cmdline.option("posboot.results")

locus.range <- cmdline.strings("posboot.between")
loci=bagpipe.define.posboot.loci(h, locus.range)
h <- happy.init.reserve(h, memory.limit.Mb=cmdline.numeric("memory"), auto.reserve=TRUE)
boot.results=bagpipe.posboot.scan(h, loci, num.boots, bootfile, save.every=1)
boot.summary=make.posboot.summary(h, loci, boot.results)
write.delim(file=cmdline.string("posboot.summary"), boot.summary)

ci50=rbind(
  make.posboot.summary.ci(h, boot.summary, prob=0.50, score="LOD"),
  make.posboot.summary.ci(h, boot.summary, prob=0.50, score="modelcmp"))
ci80=rbind( 
  make.posboot.summary.ci(h, boot.summary, prob=0.80, score="LOD"),
  make.posboot.summary.ci(h, boot.summary, prob=0.80, score="modelcmp"))
ci95=rbind(
  make.posboot.summary.ci(h, boot.summary, prob=0.95, score="LOD"),
  make.posboot.summary.ci(h, boot.summary, prob=0.95, score="modelcmp"))
write.delim(file=cmdline.string("posboot.quicksummary"), rbind(ci50,ci80,ci95))

if (cmdline.has.option("posboot.plot"))
{
	plot.scale <- cmdline.string("plot.scale", default="cM")
	loci.lim <- c(loci[1], tail(loci,1))
	chr <- happy.get.chromosome(h, loci[1])
	
	pdf(file=cmdline.string("posboot.plot"), width=6, height=5)

  for (score in c("LOD", "modelcmp"))
  {
    y=boot.summary[,paste("times.max.", sep="", score)]
    max.y=max(y)
    qy=boot.summary[,paste("cumfrac.max.", sep="", score)]
    qy=ifelse(qy>0.5, 1-qy, qy)

    i50=as.integer(ci50[ci50$score==score,c("idx.start","idx.end")])
    i80=as.integer(ci80[ci80$score==score,c("idx.start","idx.end")])
    i95=as.integer(ci95[ci95$score==score,c("idx.start","idx.end")])

    fill=rep("white", length(y))
    fill[i95[1]:i95[2]] = "gray80"
    fill[i80[1]:i80[2]] = "gray50"
    fill[i50[1]:i50[2]] = "black"

  	happy.plot.intervals(h, loci=loci, y=y,
  			loci.lim=loci.lim,
  			ylim=c(0,max.y), fill=fill,
  			ylab="Frequency at peak top",
  			xlab=paste("Position on chromosome ", chr," (",plot.scale,")",sep=""),
  			main=paste(score, "maxima from", sum(y), "positional bootstraps"),
  			scale=plot.scale)
  }
  dev.off()
}
