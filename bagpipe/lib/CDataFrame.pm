#Copyright 2012 William Valdar
#This file is part of bagpipe.

#bagpipe is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#bagpipe is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with bagpipe.  If not, see <http://www.gnu.org/licenses/>.

use strict;
package CDataFrame;
use English;
use VarType;
use StdDefs;
use Array;
use Array2d;
use Assert;
use NamedArgs;
use String;
use VarType qw(AsArray);
use Algorithm qw(ForEach);
use Debug qw(DumpObject);

my $COLUMN_ORDER_FIELD = '__COLUMN_ORDER';

sub CDataFrame::new
{
    my($class, $hArgs) = NamedArgs::Parse(\@ARG, 1,
            {'optional'=>
                 {
                     'format'      => 'normal',
                     'header'      => false,
                     'hash'        => null,
                     'matrix'      => null,
                     'pad_string'  => null,
                     'stream'      => null,
                     'strip.white' => true,
                     'hrmatrix'    => null, # deprecated
    #S/R read.table: 'fill'        => true,
    #                'blank.lines.skip'=> true,
    #                'dec'         => ".",

                 }
            });
    my $this = bless {
        '__COLUMN_ORDER'=>{},
        }, $class;


    #---------------------------------------
    # deprecated part
    if ($hArgs->{'hrmatrix'})
    {
        $hArgs->{'matrix'} = $hArgs->{'hrmatrix'};
        $hArgs->{'header'} = true;
        Warn("deprecated argument \'hrmatrix\'");
    }
    #----------------------------------------

    if ($hArgs->{'matrix'})
    {
        $this->_readMatrix($hArgs);
    }
    elsif ($hArgs->{'stream'})
    {
        $this->_readStream($hArgs);
    }
    elsif ($hArgs->{'hash'})
    {
        $this->_readHash($hArgs);
    }
    return $this;
}

sub CDataFrame::_readStream($$)
{
    my($this, $hArgs) = @ARG;

    if ($hArgs->{'format'} =~ m/normal/i)
    {
        $hArgs->{'sep'} = '\s';
        $hArgs->{'quote'} = "\"";
        $this->_parseStream($hArgs);
    }
    elsif ($hArgs->{'format'} =~ m/delim/i)
    {
        $hArgs->{'sep'} = "\t";
        $hArgs->{'quote'} = "\"";
        $this->_parseStream($hArgs);
    }
    elsif ($hArgs->{'format'} =~ m/splus/i)
    {
        $this->_readStreamSplus($hArgs);
    }
    elsif ($hArgs->{'format'} =~ m/csv/i)
    {
        $hArgs->{'sep'} = ",";
        $hArgs->{'quote'} = "\"";
        $this->_parseStream($hArgs);
    }
    else
    {
        Fatal("Unsupported format \'".$hArgs->{'format'}."\'");
    }
}

sub CDataFrame::_parseStream($$)
{
    my($this, $hArgs) = @ARG;

    my $ist   = $hArgs->{'stream'};
    my $quote = quotemeta $hArgs->{'quote'};
    my $sep   = $hArgs->{'sep'};

    my $aRows = [];
    while (my $line = $ist->getline)
    {
        next unless $line =~ m/\S/;
        $line = String::Trim($line);

        # avoid dealing with quotes if possible
        if ($line !~ m/$quote/)
        {
            #warn("no quote\n");
            push @$aRows, [split m/$sep/o, $line];
            next;
        }

        # deal with quotes
        my $row = [];
        unless (Number::IsEven(String::CountChar($line, $quote)))
        {
            Fatal("Odd number of quote characters [$quote] in [$line]");
        }

        while ($line =~ m/\S/)
        {
            if ($line =~ s/^([^$quote]+)$sep*//)
            {
                push @$row, split m/$sep/o, $1;
            }
            if ($line =~ s/^(${quote}[^$quote]+${quote})$sep?//)
            {
                push @$row, $1;
            }
        }
        push @$aRows, $row;

    }

    # strip white if desired
    if ($hArgs->{'strip.white'})
    {
        for my $row (@$aRows)
        {
            $row = String::TrimArray($row);
        }
    }

    $hArgs->{'matrix'} = $aRows;

    $this->_readMatrix($hArgs);
}

sub CDataFrame::_readStreamSplus($$)
{
    my($this, $hArgs) = @ARG;

    my $ist = $hArgs->{'stream'};
    my $aRows = [];

    if ($hArgs->{'header'})
    {
        my $line = null;
        while (($line = $ist->getline) !~ m/\S/){}
        push @$aRows, _ParseSplusLine($line);
    }
    while (my $line = $ist->getline)
    {
        next unless $line =~ m/\S/;
        my $lineArray = _ParseSplusLine($line);
        shift @$lineArray;
        push @$aRows, $lineArray;
    }
    $hArgs->{'matrix'} = $aRows;

    $this->_readMatrix($hArgs);
}

sub CDataFrame::_readMatrix($$)
{
    my($this, $hArgs) = @ARG;

    my $aRows = $hArgs->{'matrix'};

    # get max number of columns
    my $maxRowLength = 0;
    my $maxRowLengthIndex = 0;
    for (my $i=0; $i<@$aRows; ++$i)
    {
        my $row = $aRows->[$i];
        if ($maxRowLength < @$row)
        {
            $maxRowLength = @$row;
            $maxRowLengthIndex = $i;
        }
    }

    # get or make col names
    my $aColNames = null;
    if ($hArgs->{'header'})
    {
        $aColNames = shift @$aRows;
        if (@$aColNames < $maxRowLength)
        {
            Fatal("Must have at least as many column headers as data columns:"
                    ." headers = ".@$aColNames.", max row length = "
                    .$maxRowLength. " at row ".($maxRowLengthIndex+1));
        }
    }
    else
    {
        $aColNames = Array::Paste(["X"], [1..scalar(@{$aRows->[0]})],
                "sep"=>null);
    }
    $this->setColOrder($aColNames);

    # pad data rows if necessary
    for my $row (@$aRows)
    {
        if ($maxRowLength > @$row)
        {
            my $pad = Array::New($maxRowLength-@$row,
                    $hArgs->{'pad_string'});
            push @$row, @$pad;
        }
    }

    # make df
    for my $colName (@$aColNames)
    {
        $this->{$colName} = [];
    }
    for my $row (@$aRows)
    {
        for (my $i=0; $i<@$row; $i++)
        {
            push @{$this->{$aColNames->[$i]}}, $row->[$i];
        }
    }
}

sub CDataFrame::_readHash($$)
{
    my($this, $hArgs) = @ARG;

    my $hash = $hArgs->{'hash'};

    while (my($key, $value) = each %$hash)
    {
        $this->addCol($key, $value);
    }
}

sub CDataFrame::addCol($$$)
{
    my($this, $name, $value, $hArgs) = NamedArgs::Parse(\@ARG, 3,
            {
                'optional' =>
                 {
                     'expand' => false,
                 }
            }
            );

    unless ($this->hasCol($name))
    {
        my $rank = undef;
        if (scalar keys %{ $this->{$COLUMN_ORDER_FIELD} })
        {
            $rank = 1 + Array::Max([values %{$this->{$COLUMN_ORDER_FIELD}}]);
        }
        else
        {
            $rank = 1;
        }
        $this->{$COLUMN_ORDER_FIELD}{$name} = $rank;
    }

    # expand if necessary
    if ($hArgs->{'expand'})
    {
        return $this->addExpandingCol($name, $value);
    }

    my $array = VarType::AsArray($value);
    if ($this->getNumCols and $this->getNumRows != @$array)
    {
        Fatal("cannot add array of size ".scalar(@$array)
                ." to CDataFrame of size ".$this->getNumRows);
    }
    $this->{$name} = $array;
    return $this;
}

sub CDataFrame::addExpandingCol($$$)
{
    my($this, $name, $array) = @ARG;
    $array = VarType::AsArray($array);

    if ($this->getNumRows < @$array)
    {
        Fatal("array is too large to expand");
    }
    my $aExpanded = Array::Rep(
            $array,
            'length' => $this->getNumRows,
            'each' => 1
            );
    $this->{$name} = $aExpanded;
    return $this;
}

sub CDataFrame::addRow($$)
{
    my($this, $hash) = @ARG;
    unless ($this->getNumCols == scalar keys %$hash)
    {
        Fatal("added rows must have same column names as dataframe:\n"
                ."is    => ".join("|", sort keys %$hash)."\n"
                ."ought => ".join("|", sort keys %$this)."\n");
    }
    while (my($key, $value) =  each %$hash)
    {
        unless (exists $this->{$key})
        {
            Fatal("could not find column named \'$key\' in dataframe");
        }
        push @{$this->{$key}}, $value;
    }
    return $this;
}

sub CDataFrame::cbind($$)
{
    my($this, $df, $hArgs) = NamedArgs::Parse(\@ARG, 2,
            {
                'optional' =>
                 {
                     'expand' => false,
                 }
            });
    for my $col (@{$df->getColOrder})
    {
        $this->addCol($col, $df->getCol($col), 'expand'=>$hArgs->{'expand'});
    }


}

sub CDataFrame::clone($)
{
    my $this = shift;
    my $copy = {};
    while (my($key, $value) = each %$this)
    {
        if ($key eq $COLUMN_ORDER_FIELD)
        {
            $copy->{$key} = Hash::Copy($value);
        }
        else
        {
            $copy->{$key} = [@$value];
        }
    }
    return bless $copy, "CDataFrame";
}

sub CDataFrame::crop
{
    my($this, %hArg) = @ARG;

    # rm null column
    delete $this->{''} if exists $this->{''};

    # search for trailing blanks
    my $maxLastIndex = 0;
    for my $name (keys %$this)
    {
        my $array = $this->{$name};
        my $lastIndex = Array::RevFindIf($array,
                sub
                {
                    my $str = shift;
                    return false if not defined $str;
                    if ($hArg{'rmnulls'} and 0==length($str))
                    {
                        return false;
                    }
                    return true;
                }
                );
        if (Array::NOT_FOUND eq $lastIndex)
        {
            $lastIndex = scalar(@$array) - 1;
        }
        if ($lastIndex > $maxLastIndex)
        {
            $maxLastIndex = $lastIndex;
        }
    }

    # crop trailing blanks
    while (my($name, $array) = each %$this)
    {
        Array::Crop($array, 0, $maxLastIndex);
    }

    return $this;
}


sub CDataFrame::filterRows
{
    my ($this, %hFilter) = @ARG;

    my $nrow = $this->getNumRows;
    my $mask = Array::Rep(true, 'length'=>$nrow);

    while (my($name, $value) = each %hFilter)
    {
        my $array = $this->getCol($name);
        for (my $i=0; $i<$nrow; ++$i)
        {
            $mask->[$i] = false if $array->[$i] ne $value;
        }
    }
    return $this->mask($mask);
}

sub CDataFrame::getCell($$$)
{
    my($this, $rowIndex, $colName) = @ARG;
    unless ($this->hasCell($rowIndex, $colName))
    {
        Fatal("No such cell ($rowIndex,\'$colName\')");
    }
    return $this->{$colName}[$rowIndex];
}

sub CDataFrame::getCol($$)
{
    my($this, $name) = @ARG;
    unless (exists $this->{$name})
    {
        Fatal("Column \'$name\' does not exist");
    }
    return $this->{$name};
}

sub CDataFrame::getColNames($)
{
    my $this = shift;
    my $aNames = [];
    for my $name (sort keys %$this)
    {
        next if $name eq $COLUMN_ORDER_FIELD;
        push @$aNames, $name;
    }
    return $aNames;
}

sub CDataFrame::getColOrder($)
{
    my $this = shift;
    my $hKnown2Rank = $this->{$COLUMN_ORDER_FIELD};

    my $aKnown   = [];
    my $aUnknown = [];
    for my $name (@{ $this->getColNames })
    {
        if (exists $hKnown2Rank->{$name})
        {
            push @$aKnown, $name;
        }
        else
        {
            push @$aUnknown, $name;
        }
    }

    my $aNames = [
            sort
            { $hKnown2Rank->{$a} <=> $hKnown2Rank->{$b} }
            @$aKnown
            ];

    push @$aNames, sort @$aUnknown;
    return $aNames;
}

sub CDataFrame::getNumCols($)
{
    my $this = shift;
    return (scalar keys %$this) -1;
}

sub CDataFrame::getNumRows($)
{
    my $this = shift;
    my $aCols = $this->getColNames;
    if (@$aCols)
    {
        return scalar(@{ $this->{$aCols->[0]} });
    }
    return 0;
}

sub CDataFrame::getRow
{
    my $this = shift @ARG;
    my $i    = shift @ARG;
    my $cols = (@ARG) ? shift @ARG : $this->getColOrder;

    unless ($this->hasRow($i))
    {
        Fatal("Cannot get non-existant row $i\n");
    }

    my $row = [];
    for my $col (@$cols)
    {
        push @$row, $this->{$col}[$i];
    }
    return $row;
}

sub CDataFrame::getRows($$)
{
    my($this, $aIndices) = @ARG;

    my $subDf = new CDataFrame;
    for my $field (@{$this->getColNames})
    {
        $subDf->addCol($field,
                Array::Subset($this->getCol($field), $aIndices));
    }
    $subDf->setColOrder($this->getColOrder);

    return $subDf;
}

sub CDataFrame::hasCell($$$)
{
    my($this, $rowIndex, $colName) = @ARG;
    return ($this->hasRow($rowIndex) and $this->hasCol($colName))
            ? true : false;
}

sub CDataFrame::hasCol($$)
{
    my($this, $name) = @ARG;
    return exists $this->{$name};
}

sub CDataFrame::hasRow($$)
{
    my($this, $rowIndex) = @ARG;
    return 0 <= $rowIndex and $rowIndex < $this->getNumRows;
}

sub CDataFrame::isEmpty($)
{
    my $this = shift;
    return 0==$this->getNumCols;
}


sub CDataFrame::mask($$)
{
    my($this, $mask) = @ARG;

    unless (scalar(@$mask) == $this->getNumRows)
    {
        Fatal("mask must have as many elements as data frame has rows");
    }

    my $maskedDf = new CDataFrame;
    for my $field (@{$this->getColNames})
    {
        $maskedDf->addCol($field,
                Array::Mask($this->getCol($field), $mask));
    }
    $maskedDf->setColOrder($this->getColOrder);
    return $maskedDf;
}

sub CDataFrame::rbind($$)
{
    my($a, $b, $hArgs) = NamedArgs::Parse(\@ARG, 2,
            {
                'optional' =>
                 {
                     'expand' => false,
                 }
            }
            );

    # deal with trivial cases
    return $a->clone if $b->isEmpty;
    return $b->clone if $a->isEmpty;

    # see if column names match...
    unless (Array::Equals($a->getColNames, $b->getColNames))
    {
        return $a->_rbindExpand($b) if $hArgs->{'expand'};
        Fatal("CDataFrames must have the same column names");
    }

    # bind tables together
    my $c = new CDataFrame;
    for my $name (@{$a->getColNames})
    {
        $c->addCol($name, [@{$a->getCol($name)}, @{$b->getCol($name)}]);
    }
    return $c;
}

sub CDataFrame::_rbindExpand($$)
{
    my($a, $b) = @ARG;

    my($notB, $notA, $comm) = Array::Diff($a->getColNames, $b->getColNames);

    ForEach($notB, sub{$b->addExpandingCol($ARG[0], null)});
    ForEach($notA, sub{$a->addExpandingCol($ARG[0], null)});

    return $a->rbind($b);
}

sub CDataFrame::rqbind($$)
{
    my($this, $df) = @ARG;

    return $this if $df->isEmpty;

    unless ($this->isEmpty
            or
            Array::Equals($this->getColNames, $df->getColNames))
    {
        Fatal("CDataFrames must have the same column names:\n[",
                join(@{$this->getColOrder}, ","), "]\n[",
                join(@{$df->getColOrder}, ","), "]");
    }

    for my $name (@{$df->getColNames})
    {
        push @{$this->{$name}}, @{$df->{$name}};
    }
    return $this;
}

sub CDataFrame::removeCol($$)
{
    my($this, $colName) = @ARG;

    my $array = $this->getCol($colName);

    delete $this->{$colName};
    delete $this->{$COLUMN_ORDER_FIELD}{$colName};

    return $array;
}

sub CDataFrame::removeCols($$)
{
    my($this, $aColNames) = @ARG;
    for my $colName (@$aColNames)
    {
        $this->removeCol($colName);
    }
}

sub CDataFrame::renameCol($$$)
{
    my($this, $oldName, $newName) = @ARG;

    unless ($this->hasCol($oldName))
    {
        Fatal("Cannot rename non-existant column \"$oldName\"");
    }

    $this->{$newName} = delete $this->{$oldName};
    $this->{$COLUMN_ORDER_FIELD}{$newName} =
            delete $this->{$COLUMN_ORDER_FIELD}{$oldName};
    return $this;
}

sub CDataFrame::renameCols($$)
{
    my($this, $hOld2New) = @ARG;

    while (my($old, $new)=each %$this)
    {
        $this->renameCol($old, $new);
    }
    return $this;
}

sub CDataFrame::setCol($$$)
{
    my($this, $name, $value) = @ARG;
    return $this->addCol($name, $value);
}

sub CDataFrame::setCell($$$$)
{
    my($this, $rowIndex, $colName, $value) = @ARG;
    unless ($this->hasCell($rowIndex, $colName))
    {
        Fatal("No such cell ($rowIndex,\'$colName\')");
    }
    $this->{$colName}[$rowIndex] = $value;
}

sub CDataFrame::setColOrder($$)
{
    my($this, $aColNames) = @ARG;
    $this->{$COLUMN_ORDER_FIELD} = {};
    my $count = 0;
    for my $name (@$aColNames)
    {
        $this->{$COLUMN_ORDER_FIELD}{$name} = ++$count;
    }
}

sub CDataFrame::transpose($$)
{
    my($this, $pivot) = @ARG;

    unless ($this->hasCol($pivot))
    {
        Fatal("Pivot column \"$pivot\" does not exist");
    }

    my $df = new CDataFrame;
    my $oldCols = Array::Exclude($this->getColOrder, $pivot);
    $df->addCol($pivot, $oldCols);
    for (my $i=0; $i<$this->getNumRows; ++$i)
    {
        my $newCol = $this->getCell($i, $pivot);
        my $data   = $this->getRow($i, $oldCols);
        $df->addCol($newCol, $data);
    }
    return $df;
}

sub CDataFrame::write
{
    my($this, $hArgs) = NamedArgs::Parse(\@ARG, 1,
            {
                'required' => [qw(stream)],
                'optional' =>
                 {
                     'format' => 'normal',
                     'header' => true,
                     'cols' => null,
                 }
            }
            );

    my $matrix = $this->asMatrix(
            'header'=>$hArgs->{'header'},
            'transpose'=>false,
            'cols' => $hArgs->{'cols'},
            );

    my $ost = $hArgs->{'stream'};
    if ('normal' eq $hArgs->{'format'} or 'delim' eq $hArgs->{'format'})
    {
        for my $row (@$matrix)
        {
            $ost->print(join("\t", @$row), "\n");
        }
    }
    elsif ($hArgs->{'format'} =~ m/csv/i)
    {
        for my $row (@$matrix)
        {
            $ost->print(join(",", @$row), "\n");
        }
    }
    elsif ($hArgs->{'format'} =~ m/splus/i)
    {
        $ost->print(_MakeSplusLine($matrix->[0]), "\n");
        for (my $i=1; $i<@$matrix; $i++)
        {
            unshift @{$matrix->[$i]}, $i;
            $ost->print(_MakeSplusLine($matrix->[$i]), "\n")
        }
    }
    else
    {
        Fatal("Unsupported write format \'".$hArgs->{'format'}."\'");
    }
}

sub CDataFrame::asMatrix
{
    my($this, $hArgs) = NamedArgs::Parse(\@ARG, 1,
            {
                'optional' =>
                 {
                     'header' => true,
                     'transpose' => false,
                     'cols' => null,
                 }
            });
    my $matrixT = [];
    my $aColNames = $hArgs->{'cols'}
            ? $hArgs->{'cols'}
            : $this->getColOrder;

    for my $colName (@$aColNames)
    {
        my $rowT = Array::Copy($this->getCol($colName));
        if ($hArgs->{'header'})
        {
            unshift @$rowT, $colName;
        }
        push @$matrixT, $rowT;
    }

    if ($hArgs->{'transpose'})
    {
        return $matrixT;
    }
    else
    {
        return Array2d::Transpose($matrixT);
    }
}

sub CDataFrame::subset
{
    my($this, $hArgs) = NamedArgs::Parse(\@ARG, 1,
            {
                'optional' =>
                 {
                     'rows' => null,
                     'cols' => null,
                 }
            }
            );
    my $aWantedRows = (null ne $hArgs->{'rows'})
            ? AsArray($hArgs->{'rows'})
            : [0..($this->getNumRows-1)];
    my $aWantedCols = (null ne $hArgs->{'cols'})
            ? AsArray($hArgs->{'cols'})
            : $this->getColNames;

    my $df = new CDataFrame;
    for my $wantedCol (@$aWantedCols)
    {
        my $data = Array::Subset($this->getCol($wantedCol),$aWantedRows);
        $df->addCol($wantedCol, $data);
    }
    return $df;
}

sub _ParseSplusLine($)
{
    my $line = String::Trim(shift);

    my $array = [];
    while (true)
    {
        if ($line =~ s/^\"\"\s*//)
        {
            push @$array, null;
        }
        elsif ($line =~ s/^(\"?)([^\"]+)(\1?)\s*//)
        {
            my $leader  = $1;
            my $middle  = $2;
            my $trailer = $3;

            if ($leader eq '"')
            {
                push @$array, $middle;
            }
            else
            # for items not enclosed by ""s,
            # split on white space
            {
                push @$array, split(m/\s+/, $middle);
            }
        }
        last unless $line =~ m/\S/;
    }
    return $array;
}

sub _MakeSplusLine($)
{
    my $array = shift;
    $array = Array::Apply($array, sub{"\"".$ARG[0]."\""});
    return join(" ", @$array);
}



1;
