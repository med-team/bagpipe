#Copyright 2012 William Valdar
#This file is part of bagpipe.

#bagpipe is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#bagpipe is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with bagpipe.  If not, see <http://www.gnu.org/licenses/>.

use strict;
#==============================================================================
# MODULE: Algorithm
#======================================
# DESCRIPTION:
#

#_ Module declaration

package Algorithm;

#_ Include libraries

use English;
use StdDefs;
use Assert;
use Array;
use Exporter;
use VarType qw(IsArrayRef IsHashRef);
use base qw(Exporter);
use vars qw( @EXPORT_OK );

@EXPORT_OK = qw(
        Accumulate
        CopyIf
        Count
        CountIf
        CountString
        FindIf
        ForEach
        Generate
        MaxElement
        MinElement
        RandomShuffle
        Transform
        );

#------------------------------------------------------------------------------
# INTERFACE
#----------

#_ Public constants

use constant NOT_FOUND => null;

#_ Public functions

sub Accumulate;
# Function:
#   Accumulate the results of an operation on an array.
# ARGUMENTS:
#   EITHER
#       1. <\@> input
#       2. <real> initial value
#   OR
#       1. <\@> input
#       2. <?> initial value
#       3. <\sub> binary operation that "adds" two elements
# RETURN:
#   1. <real> or <?> accumulated value

sub CopyIf($$$);
# Function:
#   Copy elements that match a predicate from one array into another array
# ARGUMENTS:
#   1. <\@> input
#   2. <\@> output
#   3. <\sub> predicate
# RETURN:
#   1. <\@> output

sub Count($$);
# Function:
#   Count occurences of a value in an array. Uses the '==' operator to assess
#       equivalence.
# ARGUMENTS:
#   1. <\@> input
#   2. <SCALAR> value
# RETURN:
#   1. <int> count

sub CountIf($$);
# Function:
#   Count matches of a predicate in an array.
# ARGUMENTS:
#   1. <\@> input
#   2. <\sub> predicate
# RETURN:
#   1. <int> count

sub CountString($$);
# Function:
#   Count occurences of a value in an array. Uses the 'eq' operator to assess
#       equivalence.
# ARGUMENTS:
#   1. <\@> input
#   2. <SCALAR> value
# RETURN:
#   1. <int> count

sub FindIf($$);
# Function:
#   Find first match of a predicate in an array.
# ARGUMENTS:
#   1. <\@> input
#   2. <\sub> predicate
# RETURN:
#   1. <?> element if found, <CONSTANT> NOT_FOUND otherwise.

sub ForEach($$);
# Function:
#   Do operation for every element in an array.
# ARGUMENTS:
#   1. <\@> input
#   2. <\sub> operation
# RETURN:
#   1. <\sub> operation

sub Generate($$$);

sub Transform($$$);
# Function:
#   Apply an operation to every element in an array
# ARGUMENTS:
#   1. <\@> input
#   2. <\@> output (may be the same as input)
#   3. <\sub> operation
# RETURN:
#   1. <\@> output
# SEE ALSO:



#------------------------------------------------------------------------------
# IMPLEMENTATION 
#---------------

sub Accumulate
{
    if (2 == @ARG)
    {
        return _Accumulate_2Args(@ARG);
    }
    elsif (3 == @ARG)
    {
        return _Accumulate_3Args(@ARG);
    }
    WrongNumArgsError();
}

sub _Accumulate_2Args($$)
{
    my ($array, $init) = @ARG;
    for my $elem (@$array)
    {
        $init += $elem;
    }
    return $init;
}

sub _Accumulate_3Args($$$)
{
    my($array, $init, $binOp) = @ARG;
    for my $elem (@$array)
    {
        $init += &$binOp($init, $elem);
    }
    return $init;
}

sub CopyIf($$$)
{
    my($aIn, $aOut, $unOp) = @ARG;
    
    for my $elem (@$aIn)
    {
        if (&$unOp($elem))
        {
            push @$aOut, $elem;
        }
    }
    return $aOut;
}

sub Count($$)
{
    my($aIn, $value) = @ARG;
    
    my $count = 0;
    for my $elem (@$aIn)
    {
        $count++ if $elem == $value;
    }
    return $count;
}

sub CountIf($$)
{
    my($aIn, $op) = @ARG;
    
    my $count = 0;
    for my $elem (@$aIn)
    {
        if (&$op($elem))
        {
            $count++;
        }
    }
    return $count;
}

sub CountString($$)
{
    my($aIn, $value) = @ARG;
    
    my $count = 0;
    for my $elem (@$aIn)
    {
        $count++ if $elem eq $value;
    }
    return $count;
}

sub FindIf($$)
{
    my($array, $pred) = @ARG;
    for my $elem (@$array)
    {
        if (&$pred($elem))
        {
            return $elem;
        }
    }
    return NOT_FOUND;
}

sub ForEach($$)
{
    my($array, $op) = @ARG;
    for my $elem (@$array)
    {
        &$op($elem);
    }
    return $op;
}

sub Generate($$$)
{
    my($array, $nElem, $func) = @ARG;
    
    return Array::Generate($array, $nElem, $func);
}

sub MaxElement    
{
    1 == @ARG || 2 == @ARG or WrongNumArgsError();
    
    my $aIn = $ARG[0];
    my $binPred = (2 == @ARG)   ? $ARG[1]
                                : sub{$ARG[0] <=> $ARG[1]};
    my $max = $aIn->[0];
    for (my $i=1; $i<@$aIn; $i++)
    {
        if (-1 == &$binPred($max, $aIn->[$i]))
        {
            $max = $aIn->[$i];
        }
    }
    return $max;
}

sub MinElement
{
    1 == @ARG || 2 == @ARG or WrongNumArgsError();

    my $aIn = $ARG[0];
    my $binPred = (2 == @ARG)   ? $ARG[1]
                                : sub{$ARG[0] <=> $ARG[1]};
    my $min = $aIn->[0];
    for (my $i=1; $i<@$aIn; $i++)
    {
        if (1 == &$binPred($min, $aIn->[$i]))
        {
            $min = $aIn->[$i];
        }
    }
    return $min;
}
 
sub RandomShuffle($)
{
    return Array::Shuffle($ARG[0]);
}
    
sub Transform($$$)
{
    my($inputCont, $outputCont, $rsub) = @ARG;
    
    if (IsArrayRef($inputCont) && IsArrayRef($outputCont))
    {
        return Transform_array($inputCont, $outputCont, $rsub);
    }
    elsif (IsHashRef($inputCont) && IsHashRef($outputCont))
    {
        return Transform_hash($inputCont, $outputCont, $rsub);
    }
    else
    {
        Fatal("non-container type passed to function");
    }
}

sub Transform_hash($$$)
{
    my($hIn, $hOut, $op) = @ARG;
    
    while (my($key, $val) = each %$hIn)
    {
        $hOut->{$key} = &$op($val);
    }
    return $hOut;
}

sub Transform_array($$$)
{
    my($aIn, $aOut, $op) = @ARG;
    
    return Array::Transform($aIn, $aOut, $op);
}

#====
# END
#==============================================================================
true;
__END__

=head1 Module

Algorithm

=head1 Description

Loops can be more confusing than they're worth, especially when they perform trivial tasks. This module is loosely based on the C++ algorithm.h module, which aims to cut down the number of explicit loops in code.

=head1 Notes

=over

=item predicate

This is a function that returns nonzero iff its argument satisfy a condition. If it take 1 argument, it is a unary predicate, 2 arguments, a binary predicate, and so on.

=item operation

This is a function that returns a value. If it takes 1 argument, it is unary, 2 args then binary, etc.

=back

=head1 Functions

=over

=item Accumulate

 Function:
   Accumulate the results of an operation on an array.
 ARGUMENTS:
   EITHER
       1. <\@> input
       2. <real> initial value
   OR
       1. <\@> input
       2. <?> initial value
       3. <\sub> binary operation that "adds" two elements
 RETURN:
   1. <real> or <?> accumulated value

=item CopyIf

 Function:
   Copy elements that match a predicate from one array into another array
 ARGUMENTS:
   1. <\@> input
   2. <\@> output
   3. <\sub> predicate
 RETURN:
   1. <\@> output

=item Count

 Function:
   Count occurences of a value in an array. Uses the '==' operator to assess
       equivalence.
 ARGUMENTS:
   1. <\@> input
   2. <SCALAR> value
 RETURN:
   1. <int> count

=item CountIf

 Function:
   Count matches of a predicate in an array.
 ARGUMENTS:
   1. <\@> input
   2. <\sub> predicate
 RETURN:
   1. <int> count

=item CountString

 Function:
   Count occurences of a value in an array. Uses the 'eq' operator to assess
       equivalence.
 ARGUMENTS:
   1. <\@> input
   2. <SCALAR> value
 RETURN:
   1. <int> count

=item FindIf

 Function:
   Find first match of a predicate in an array.
 ARGUMENTS:
   1. <\@> input
   2. <\sub> predicate
 RETURN:
   1. <?> element if found, <CONSTANT> NOT_FOUND otherwise.

=item ForEach

 Function:
   Do operation for every element in an array.
 ARGUMENTS:
   1. <\@> input
   2. <\sub> operation
 RETURN:
   1. <\sub> operation

=item Transform

 Function:
   Apply an operation to every element in an array
 ARGUMENTS:
   1. <\@> input
   2. <\@> output (may be the same as input)
   3. <\sub> operation
 RETURN:
   1. <\@> output

