#!/usr/bin/perl

#Copyright 2012 William Valdar
#This file is part of bagpipe.

#bagpipe is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#bagpipe is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with bagpipe.  If not, see <http://www.gnu.org/licenses/>.

# TODO: replace window.radius with peak.separation everywhere!!!
BEGIN
{
	unless ($ENV{'BAGPIPE_LIBS'})
	{
		die "Error: shell variable \$BAGPIPE_LIBS must be defined.\n";  
	}
}
# General packages
use strict;
use English;
use Data::Dumper;
use Getopt::Long;
use warnings;
# My packages
use lib $ENV{'BAGPIPE_LIBS'};
use Assert;
use StdDefs;
use CConfigFile;
use CDataFrame;
use File::Copy;
use File::Path;
use FileIoHelper qw(OpenFilesForReading OpenFilesForWriting
		FileNotEmpty FileEmptyOrAbsent FileExists
		GetFileBasename GetFileSuffix
		GetDirContents );
    
# Global variables accessed by functions
my $DRY_RUN = false;
my $CLEAN_UP = true;
my $DEVEL    = false;
my $SHOW_WARNINGS = true;
my $CAUGHT_R_INPUT_ERROR_PATTERN = "Bagphenotype .* Error";

# Documentation ----------------------------------------

my $VERSION_STRING = <<EOV;
--------------------------------------------------------------------
Bagpipe - a pipeline for genetic association using HAPPY genome data
Version: 2012_02_14
Author: William Valdar <william.valdar\@unc.edu>.
Cite: Valdar et al (2009) Genetics 182(4):1263-77.
Please note: this is software is distributed under a GNU copyleft
license, is not formally validated, and comes with no guarantees
whatsoever.
--------------------------------------------------------------------
EOV

my $HELP_USAGE_STRING =<<EOU;
Type 'bagpipe -h' for summary usage
     'bagpipe --help' for detailed usage
     'bagpipe --examples' for examples
EOU

my $BASIC_USAGE_STRING = <<EOU;
bagpipe configfile [-scan] [-perm] [-perm_collate]
    [-nullsim] [-nullsim_collate] [-plot] [other options]
EOU

my $SUMMARY_USAGE_STRING = <<EOU;
Usage:
  bagpipe configfile [-action(s)] [-option(s)=argument]

actions: scan, perm, perm_collate, nullsim, nullsim_collate,
    plot

options: locus_groups, outdir, scandir
    plot_height, plot_width, plot_scale, plot_type,
    plot_score, plot_tailprob
    memory, overwrite
EOU

my $DETAILED_USAGE_STRING = << 'EOU';

Usage:
  bagpipe configfile [options ...]

Requirements:
Config file: a file of configuration parameters
  HAPPY genome cache: a directory containing cached HAPPY matrices
  Phenotype file: a tab delimited table with phenotype and covariate
          values for each animal. The table must include
          1) a column SUBJECT.NAME giving the id of the animal in
          the corresponding HAPPY data;
          2) a column with the name of the phenotype as listed in the
          Phenotype column of the model menu file;
          3) columns corresponding to any covariates used in the
          model menu formulae.
Key:
n    - an integer
d    - a decimal
s    - a string
b    - 1 for true and 0 for false.
xL   - a comma separated list of type x
locus.group - either the name of a chromosome (eg, 1 or X) or a file of marker names.

Actions:
  scan             - Genome scans
  perm             - Permutations
  perm_collate     - Calculate permutation thresholds
  nullsim          - Null simulations (not yet implemented)
  nullsim_collate  - Calculate null simulation thresholds (not yet implemented)
  plot             - Plot QTL scans to pdf file.

  scanloci=sL      - [Experimental] Scan individual loci, saving all fit information
  posboot=sL       - [Experimental] Perform k positional bootstraps between marker
                      intervals m1 and m2, where s=m1,m2,k

Settings:
  outdir           - Directory for output. Default is ./
  scandir          - Directory for output of scan data files within outdir (./)
  locus_groups=sL  - Run analyses on certain locus groups only [Default: do all in configfile]
  save.at.loci=sL  - [Experimental] Save model fit when scanning at specified loci.
  tailprobs=dL     - Calculate nullscan tail probabilities (ie, significance thresholds).
                     Eg, 0.05,0.01. [Default nL=0.001,0.01,0.05,0.1,0.2]

Plotting:
  plot_box         - Draw box around each plot
  plot_scale=s     - Scale used for plot summary s={cM,bp,Mb}
  plot_score=s     - Specify locus score to plot s={LOD,modelcmp}
  plot_type=s      - s = {chromosomes,genome}
  plot_tailprob=sL - Include threshold lines as calculated by the *_collate options.
                    Each string s should have format <nullscantype>_<d>, where
                    nullscantype={perm,nullsim} and <d> is the tailprob looked up in the
                    corresponding thresh file. Eg, s=perm_0.05 plots the 0.05 threshold
                    calculated by permutation.
  plot_height=d    - Notional height of plotsummary output in inches
  plot_width=d     - Notional width of plotsummary output in inches     

Performance:
  cleanup          - Delete Rout files that do not report errors or warnings
  dryrun           - Print commands but do not execute them
  memory=d         - Store marker matrices in memory upto n Mb [default n=0]
  nomenuconfig     - Write the complete config file for an analysis
                ie, incorporating all the information from the menu file
                but minus the menu file
  overwrite=b      - Overwrite existing analysis files [Default overwrite=0]

Help:
  examples         - Give examples
  example_config   - Write an example config file to config.example

EOU

# Functions --------------------------------------------

sub WriteBlankConfigFile($)
{
    my $ost = shift;

    $ost->print(<<END_OF_CONFIG);
genome.cache.dir ../SaveMatricesFromHAPPY	
phenotype.file	./PhenotypesPlusCovariates.txt	
analysis.id		MyPhenotypeModelName
scan.formula.null	anxiety.score ~ sex
scan.formula.test   anxiety.score ~ sex + interval.additive(THE.LOCUS)
scan.function	linear
num.nullscans 200
nullscan.seed 1
locus.groups 1,2,X,FileOfOtherMarkers.txt
END_OF_CONFIG
}

sub ErrorUsage($)
{
  my $errorMessage=shift;
  print STDERR "$errorMessage\n$HELP_USAGE_STRING\n";
}

sub BasicUsage()
{
  print STDERR "$VERSION_STRING\n$BASIC_USAGE_STRING\n$HELP_USAGE_STRING\n";
}

sub SummaryUsage()
{
  print STDERR "$VERSION_STRING\n$SUMMARY_USAGE_STRING\n$HELP_USAGE_STRING\n";
}

sub LongUsage()
{
  print STDERR "$VERSION_STRING\n$DETAILED_USAGE_STRING\n";
}

sub PrintExamples()
{
	print << 'EOE';
Bagpipe Examples:

bagpipe configfile --scan
    Run genome scans on the locus.groups listed in configfile

bagpipe config --scan --locus_groups=1,2
    Scan only chr 1 and 2.

bagpipe configfile --scan --dryrun
    Write commands to screen but do not execute.

bagpipe config --perm_collate --tailprobs=0.025
    Perform permutations and calculate only the upper p=0.025 quantile from an existing set of permutations

bagpipe configfile --scan --perm --perm_collate --plot --plot_score=modelcmp --plot_tailprob=perm_0.05,perm_0.01
    Scan all locus groups, do permutation scans, calculate thresholds a
	
EOE
}

sub SystemR
{
    my ($cmd, $rout, $errorMessage, $force) = @ARG;
    $cmd = "Rscript --vanilla $cmd "
      .($DEVEL ? "--devel " : "")
      ."> $rout 2>&1";
    if ($DRY_RUN and not $force)
    {
		  print "$cmd\n";
      return;
    }
    print "Executing: $cmd\n";
    system($cmd);
    if (my $msg = CheckRout($rout, $CLEAN_UP))
    {
        if ($msg =~ m/$CAUGHT_R_INPUT_ERROR_PATTERN/)
        {
            die "Bagphenotype Caught Error: $errorMessage. More info:\n$msg\n";
        }
        else
        {
            die "Runtime Error from R script: $errorMessage. See $rout for details.\nLast lines from $rout:\n$msg\n";
        }
    }
}

sub CheckRout($$)
{
    my($file, $cleanUp) = @ARG;
    my $ist = OpenFilesForReading($file);
    my $hasWarnings = false;
    while (my $line = $ist->getline)
    {
        if ($line =~ s/.*($CAUGHT_R_INPUT_ERROR_PATTERN.*)/$1/)
        {
            return $line;
        }
        if ($line =~ m/Error/ or $line =~ m/Segmentation\sfault/)
        {
            return "  ". join("  ",$ist->getlines);
        }
        if ($line =~ m/Warning/)
        {
            $hasWarnings = true;
        }
    }
    $ist->close;
	if ($cleanUp)
	{
		unless ($hasWarnings and $SHOW_WARNINGS)
		{
			unlink $file;
		}
	}
    return 0;
}

sub GetOptionFromConfig
{
	my($config, $arg, $aDeprecated) = @ARG;
	
	if ($config->has($arg))
	{
		return $config->get($arg);
	}
	if (defined $aDeprecated)
	{
		for my $darg (@$aDeprecated)
		{
			if ($config->has($darg))
			{
				warn "Deprecated option `'$darg`'. Use `'$arg`' instead\n";
				return $config->get($darg);
			}
		}
	}
	die "Could not find option $arg";
}	
	

# Main program ---------------------------------

my $R_SOURCE_DIR =  $ENV{'BAGPIPE_LIBS'};
my $OVERWRITE    = 0;
my $memory       = 0;
my $cpus         = 1;
my $outDir  = "./";
my $scanOutDir = "./SCANS/";
my $nullOutDir = "./NULLSCANS/";
my $peakOutDir = "./";
my $plotScale = "cM";
my $plotScore = "modelcmp";
my $plotType  = "chromosomes";
my $plotWidth = 12;
my $plotHeight = 6;
my %options = ();
my $tailprobs = "0.001,0.01,0.05,0.1,0.2";

if (0==@ARGV)
{
  BasicUsage();
  exit(1);
}

my $correctArgList	= GetOptions(\%options,
    'h',
    'help',
	  'examples',
	  'example_config',
	  # Directories for input and output
    'rsourcedir=s'   => \$R_SOURCE_DIR,
    'outdir=s'       => \$outDir,
    'scandir=s'      => \$scanOutDir,
    'nulldir=s'      => \$nullOutDir,
    'peakdir=s'      => \$peakOutDir,
  	# analysis options
  	'locus_groups=s',
  	'overwrite=i'	 => \$OVERWRITE,
  	# Performance options
    'cleanup=i'      => \$CLEAN_UP,
    'dryrun',
    'memory=i'       => \$memory,
  	'cpus=i'		 => \$cpus,
  	'warnings=i'     => \$SHOW_WARNINGS,
    'tailprobs=s'    => \$tailprobs,
  	# Actions
    'scan',
    'scanloci=s',
    'perm',      
    'perm_collate',
    'nullsim',
    'nullsim_collate',
  	'nullsimperm',
    'nullsimperm_collate',
    'posboot_between=s',
    'plot',    
    # Plotting cosmetics
    'plot_box',
    'plot_height=f'  => \$plotHeight,
    'plot_scale=s'    => \$plotScale,
  	'plot_score=s'   => \$plotScore,
  	'plot_tailprobs=s',
  	'plot_title=s',
  	'plot_type=s' => \$plotType,
    'plot_width=f'   => \$plotWidth,
    'qtls=s',
    # Private options
    'devel' => \$DEVEL,
    );


if ($options{'h'})
{
  SummaryUsage();
  exit(0);	
}
if ($options{'help'})
{
  LongUsage();
  exit(0);
}
if ($options{'examples'})
{
	PrintExamples();
	exit(0);
}
if ($options{'example_config'})
{
	my $configFile = "./config.example";
  WriteBlankConfigFile(OpenFilesForWriting($configFile));
	print "Example config file written to $configFile\n";
	exit(0);
}
if (not $correctArgList or 1!=@ARGV)
{
    ErrorUsage(1!=@ARGV ? "Must specify configfile" : $ERRNO );
    exit(1);
}
$DRY_RUN = true if $options{'dryrun'};

my $scanDir = "$outDir/$scanOutDir/";
my $hNullscanProperties = {
	'perm' => {
		'dir' => "$outDir/$nullOutDir",
		'type'=>"permute.scan",
		'suffix' => "permscan",
	},
	'nullsim' => {
		'dir' => "$outDir/$nullOutDir",	
		'type'=>"nullsim.scan",
		'suffix' => "nullsimscan",
	},
	'nullsimperm' => {
		'dir' => "$outDir/$nullOutDir",	
		'type'=>"nullsimpermute.scan",
		'suffix' => "nullsimpermscan",
	}
};

#-------------------------------
# Prescan and update config file
#-------------------------------

my $configFile = $ARGV[0];
my $config = new CConfigFile($configFile);
my $id = $config->get("analysis.id");
mkpath($outDir, 1, 0755);

# Locus Groups -------------------------------

my $aAvailableLocusGroups = [split m/,/, $config->get("locus.groups")];
my $aLocusGroups = $aAvailableLocusGroups;
if (exists $options{'locus_groups'})
{
	$aLocusGroups = [split m/,/, $options{'locus_groups'}];
	unless (Array::IsSubSet($aLocusGroups, $aAvailableLocusGroups))
	{
		die "Not all requested locus groups are listed in config file\n";
	}
}

# Single locus scans of genome --------------------------
if (exists $options{'scan'})
{

  mkpath($scanDir, true, 0755);
  for my $locusGroup (@$aLocusGroups)
  {
    my $scanFile = "$id.group_$locusGroup.scan";
    my $scanFilePathName = "$scanDir/$scanFile";

    next if FileNotEmpty($scanFilePathName) and not $OVERWRITE;

    SystemR("$R_SOURCE_DIR/scangenome.R --args"
            ." --configfile=$configFile"
            ." --locus.group=$locusGroup"
            ." --outdir=$scanDir"
            ." --outfile=$scanFile "
            ." --id=$id",
            "$outDir/$scanFile.Rout",
            "genome scan of locus group $locusGroup");

    if (FileEmptyOrAbsent($scanFilePathName) and not $DRY_RUN)
    {
        die "Genome scan failed for $locusGroup\n";
    }
  }
}

# Single locus scans of individual loci ------------------
if (exists $options{'scanloci'})
{
  my $aSloci= [split m/,/, $options{'scanloci'}];
  my $aSfiles= [map{$id."_".$ARG.".save"} @$aSloci];

  SystemR("$R_SOURCE_DIR/scanloci.R --args"
          ." --configfile=$configFile"
          ." --outdir=$outDir"
          ." --loci=".join(',', @$aSloci)
          ." --loci.files=".join(',', @$aSfiles)
          ." --id=$id",
          "$outDir/scanloci.$PID.Rout",
          "scanning individual loci with detailed output");
  # todo check for output files
}

# Null scans -----------------------------------

my $aNullScans = [];
push(@$aNullScans, 'perm') if exists $options{'perm'};
push(@$aNullScans, 'nullsim') if exists $options{'nullsim'};
push(@$aNullScans, 'nullsimperm') if exists $options{'nullsimperm'};
for my $nullscan (@$aNullScans)
{
	my $dir = $hNullscanProperties->{$nullscan}{'dir'};
	mkpath($dir, true, 0755);
	my $suffix = $hNullscanProperties->{$nullscan}{'suffix'};
	my $aScanFiles = [];
    for my $locusGroup (@$aLocusGroups)
    {
        my $scanFile = "$id.group_$locusGroup.$suffix";
		push @$aScanFiles, $scanFile;

        next if FileNotEmpty("$dir/$scanFile") and not $OVERWRITE;

        SystemR("$R_SOURCE_DIR/nullscangenome.R --args"
                ." --configfile=$configFile"
                ." --cpus=$cpus"
                ." --id=$id"
                ." --scan.type=".$hNullscanProperties->{$nullscan}{'type'}
                ." --outdir=$dir"
                ." --outfile=$scanFile"
                ." --locus.group=$locusGroup"
				        ." --memory=$memory",
                "$outDir/$scanFile.Rout",
                "Error in $nullscan scan of locus group $locusGroup");

        if (FileEmptyOrAbsent("$dir/$scanFile") and not $DRY_RUN)
        {
            die "$nullscan failed for $locusGroup\n";
        }
    }	
}

my $aNullScanCollations = [];
push(@$aNullScanCollations, 'perm') if exists $options{'perm_collate'};
push(@$aNullScanCollations, 'nullsim') if exists $options{'nullsim_collate'};
push(@$aNullScanCollations, 'nullsimperm') if exists $options{'nullsimperm_collate'};
for my $nullscan (@$aNullScanCollations)
{
	my $dir = $hNullscanProperties->{$nullscan}{'dir'};
	my $suffix = $hNullscanProperties->{$nullscan}{'suffix'};
	my $aScanFiles = [];
	for my $locusGroup (@$aAvailableLocusGroups)
	{
		my $scanFile = "$id.group_$locusGroup.$suffix";
		if (FileEmptyOrAbsent("$dir/$scanFile"))
		{
			die "Cannot collate $nullscan output: missing results for locus group $locusGroup\n";
		}
		push @$aScanFiles, $scanFile;
	}
	my $gevFile = "$outDir/$id.gev.$suffix";
	my $threshFile = "$outDir/$id.thresh.$suffix";
	SystemR("$R_SOURCE_DIR/fitgev.R --args"
	         ." --configfile=$configFile"
	         ." --infiles=".join(",", @$aScanFiles)
	         ." --indir=$dir"
	         ." --threshfile=$threshFile"
	         ." --gevfile=$gevFile"
	         ." --tailprobs=$tailprobs",
	         "$threshFile.Rout",
	         "Error in GEV fit of permutation scan");	
}

# Plotting -----------------------------------
if (exists $options{'plot'})
{
	my $aThresholds = [];
	if (exists $options{'plot_tailprobs'})
	{
		for my $string (split(m/,/, $options{'plot_tailprobs'}))
		{
			my($nullscan, $tailprob) = split(m/_/, $string);
			unless (exists $hNullscanProperties->{$nullscan})
			{
				die "Could not recognize nullscan type $nullscan in plot_tailprob argument\n";
			}
			my $threshFile = "$outDir/$id.thresh.".$hNullscanProperties->{$nullscan}{'suffix'};
			my $df = new CDataFrame(
		            'header'=> true,
		            'stream'=> OpenFilesForReading($threshFile),
		            'format'=> 'delim');
			my $p = $df->getCol("upper.tail.prob");
			my $quant = undef;
			for (my $i=0; $i<$df->getNumRows; $i++)
			{
				if ($tailprob==$p->[$i])
				{
					$quant = $df->getCell($i,$plotScore);
					last;
				}
			}
			unless (defined $quant)
			{
				die "Could not find threshold for $plotScore tail prob $tailprob in $threshFile\n";
			}
			push @$aThresholds, $quant;
		}
	}
	
	my $outFile = "$outDir/$id.scans.pdf";
	my $aScanFiles = [];
	for my $locusGroup (@$aAvailableLocusGroups)
	{
		my $scanFile = "$id.group_$locusGroup.scan";
		if (FileEmptyOrAbsent("$scanDir/$scanFile"))
		{
			warn "Skipping missing data for locus group $locusGroup\n";
			next;
		}
		push @$aScanFiles, $scanFile;
	}
	my $scanFileList = join ',', @$aScanFiles;
    SystemR("$R_SOURCE_DIR/plotscans.R --args"
            ." --configfile=$configFile"
			." --plot.height=$plotHeight"
			." --plot.width=$plotWidth"
			." --plot.type=$plotType"
			.($options{'plot_box'} ? " --plot.box" : "")
            ." --indir=$scanDir/"
            ." --infiles=$scanFileList"
			.($options{'plot_title'} ? " --title=".$options{'plot_title'} : "")
            ." --plot.scale=$plotScale"
			." --locus.score.type=$plotScore"
            ." --outfile=$outFile"
			.(@$aThresholds ? " --thresholds=".join(",", @$aThresholds) : " ")
			." --flag.missing",
            "$outDir/$id.scans.pdf.Rout",
            "Error in plot");
}

# Positional bootstrap -------------------------------

if (exists $options{'posboot_between'})
{
  my($locus1, $locus2, $numBoots) = split m/,/, $options{'posboot_between'};
  my $tag="$id.$locus1-$locus2";
  my $posbootResults="$outDir/$tag.posboot";
  my $posbootSummary="$outDir/$tag.posbootsum";
  my $posbootQuickSummary="$outDir/$tag.posbootqsum";
  my $posbootPlot="$outDir/$tag.posboot.pdf";

  SystemR("$R_SOURCE_DIR/posboot.R --args"
          ." --configfile=$configFile"
          ." --memory=$memory"
          ." --num.posboots=$numBoots"
          ." --posboot.between=$locus1,$locus2"
          ." --posboot.results=$posbootResults"
          ." --posboot.summary=$posbootSummary"
          ." --posboot.quicksummary=$posbootQuickSummary"
          ." --posboot.plot=$posbootPlot"
          ." --plot.scale=$plotScale"
          ." --id=$id",
          "$outDir/$posbootResults.Rout",
          "bootstrapping $numBoots times between $locus1 and $locus2");
}





